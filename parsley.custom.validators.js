﻿window.ParsleyConfig = {
		validators: {
			cpf: {
				fn : function ( val, req) {
					val = $.trim(val);

					val = val.replace('.','');
					val = val.replace('.','');
					cpf = val.replace('-','');
					while(cpf.length < 11) cpf = "0"+ cpf;
					var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
					var a = [];
					var b = new Number;
					var c = 11;
					for (i=0; i<11; i++){
						a[i] = cpf.charAt(i);
						if (i < 9) b += (a[i] * --c);
					}
					if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
					b = 0;
					c = 11;
					for (y=0; y<10; y++) b += (a[y] * c--);
					if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

					var result = true;
					if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) result = false;

					return result;
				},
				priority: 32
			},
			cnpj: {
				fn: function(val, req){

					var cnpj = val.replace(/[^\d]+/g,'');

					if(cnpj == '') return false;

					if (cnpj.length != 14)
						return false;

					// LINHA 10 - Elimina CNPJs invalidos conhecidos
					if (cnpj == "00000000000000" || 
						cnpj == "11111111111111" || 
						cnpj == "22222222222222" || 
						cnpj == "33333333333333" || 
						cnpj == "44444444444444" || 
						cnpj == "55555555555555" || 
						cnpj == "66666666666666" || 
						cnpj == "77777777777777" || 
						cnpj == "88888888888888" || 
						cnpj == "99999999999999"){
						return false;						
					}

					var tamanho = cnpj.length - 2,
						numeros = cnpj.substring(0,tamanho),
						digitos = cnpj.substring(tamanho),
						soma = 0,
						pos = tamanho - 7;

					for (i = tamanho; i >= 1; i--) {
						soma += numeros.charAt(tamanho - i) * pos--;
						
						if (pos < 2){
							pos = 9;						  	
						}

					}

					resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

					if (resultado != digitos.charAt(0)){
						return false;						
					}

					tamanho = tamanho + 1;
					numeros = cnpj.substring(0,tamanho);
					soma = 0;
					pos = tamanho - 7;

					for (i = tamanho; i >= 1; i--) {

						soma += numeros.charAt(tamanho - i) * pos--;

						if (pos < 2){
							pos = 9;					  	
						}
					}

					resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

					if (resultado != digitos.charAt(1)){
						return false;
					}

					return true;
				}
			},
			dateformat: { 
				fn: function (val, req)	{
					var dataRegexp = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");

					if( val.search(dataRegexp) === -1) return false;

					return true;
				}
			}, 
			maioridade: {
				fn: function (val, req)	{
					var hoje, dataComponents, ano, mes, dia, idade, dataRegexp;

					dataRegexp = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");

					if( val.search(dataRegexp) == -1) return true;

					hoje = new Date();

					dataComponents = val.split("/");

					if (dataComponents.length!=3) return false

					ano = parseInt(dataComponents[2]); 
					if (isNaN(ano)) return false 

					mes = parseInt(dataComponents[1]); 
					if (isNaN(mes)) return false 

					dia = parseInt(dataComponents[0]);
					if (isNaN(dia)||dia > 31) return false
						
					if(((mes == 4)||(mes == 6)||(mes == 9)||(mes == 11)) && (dia > 30)) return false;
					
					if ( mes == 2 ) {
						if ((dia > 28) && (ano % 4!== 0 )) return false
						if ((dia > 29) && (ano % 4 ===0 )) return false
					}	

					var dias=1000*60*60*24*365;
					var dataAniversario = new Date(ano, (mes-1), dia)

					idade = Math.floor(((hoje.getTime() - dataAniversario.getTime()) / dias));

					if(idade >= 18){
						return true;	
					} else{
						return false;
					}
				}
			},
			cep: {
				fn: function (val, req){
					var re = /^\b(\d)\1+\b$/;
    				return !re.test(val);
				}
			},
			leadsemail: {
				fn: function(val){
					var regex = new RegExp('^[a-zA-Z0-9\-\+\_\.\@]+$');

					console.log(regex.test(val))

					return regex.test(val);
				}
			},
			notest: {
				fn: function(val){
					return val.indexOf('teste') > -1 ? false : true;
				}
			}
		}
	};